'use strict';
const _ = require('lodash');

const SequelizeIncludeGenerator = function(models) {

    function buildIncludeGraph(filters, includeGraph) {
        filters = filters || {};
        filters = _formatFilters(filters);
        includeGraph = includeGraph || [];

        let output = _processGraph(includeGraph);

        function _processGraph(includeGraph, modelPath) {
            if (_.isEmpty(includeGraph)) {
                return {};
            }

            modelPath = modelPath || [];

            if (_.isArray(includeGraph)) {
                return _processIncludeArray(includeGraph, modelPath);
            } else {
                includeGraph = _wrapInModelObject(includeGraph);
                modelPath = modelPath.concat(includeGraph);

                return _.merge(
                    includeGraph,
                    _processGraph(includeGraph.include, modelPath),
                    _sequelizeModel(includeGraph.model),
                    _findFilter(modelPath)
                );
            }
        }

        function _processIncludeArray(includeArray, modelPath) {
            return {
                include: _.map(includeArray, include => {
                    return _processGraph(include, modelPath);
                })
            };
        }

        function _findFilter(modelPath) {
            const fullPath = modelPath.map(
              cur => cur.as ? cur.as : cur.model
            ).join('.')

            let value = filters[fullPath];
            if (value && value[Object.keys(value)[0]]) {
                return { where: value };
            }
            return {};
        }

        function _withRootFilters(output) {
            if (filters['']) {
                return _.merge(output, _findFilter([]));
            }
            return output;
        }

        return _withRootFilters(output);
    }

    function _wrapInModelObject(model) {
        if (_.isString(model)) {
            return { model: model };
        }
        return model;
    }

    function _sequelizeModel(modelName) {
        if (_.isEmpty(modelName)) {
            return {};
        }
        return { model: models[modelName] };
    }

    function _formatFilters(filters) {
        if (_.isEmpty(filters)) {
            return {};
        }

        let filterArray = _.map(filters, (value, modelPath) => {
            modelPath = modelPath.split('.');
            let model = modelPath.slice(0, modelPath.length - 1).join('.');
            let attribute = modelPath.slice(-1);

            let result = {};
            result[model] = {};
            result[model][attribute] = value;
            return result;
        });

        return _.reduce(filterArray, _.merge);
    }

    return {
        buildIncludeGraph: buildIncludeGraph,
        _formatFilters: _formatFilters
    };
};

module.exports = SequelizeIncludeGenerator;
