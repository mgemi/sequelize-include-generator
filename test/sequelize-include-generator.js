'use strict';

const expect = require('chai').expect;
const models = {
    ProductCategory: {
        name: 'ProductCategory'
    },
    ProductSubcategory: {
        name: 'ProductSubcategory'
    }
};

function mockSequelizeModel(modelName) {
    return {
        name: modelName
    };
}

describe('SequelizeIncludeGenerator', () => {
    let SequelizeIncludeGenerator = require('../sequelize-include-generator')(models);

    describe('#buildIncludeGraph', () => {
        let basicIncludeGraph = [];

        beforeEach(() => {
            basicIncludeGraph = [
                { model: 'ProductSubcategory', as: 'Subcategory', include: [{
                    model: 'ProductCategory'
                }] }
            ];
        });

        it('returns an Object', () => {
            let output = SequelizeIncludeGenerator.buildIncludeGraph();
            expect(output).to.eql({});
        });

        it('returns a simple where clause', () => {
            let filters = { name: 'Hightop' };

            let goodOutput = {
                where: {
                    name: 'Hightop'
                }
            };

            expect(SequelizeIncludeGenerator.buildIncludeGraph(filters))
                .to.eql(goodOutput);
        });

        it('returns basic nested results', () => {
            let filters = {
              'Subcategory.id': 77,
              'Subcategory.ProductCategory.name': 'Foo'
            };

            let goodOutput = {
                include: [{
                    model: mockSequelizeModel('ProductSubcategory'),
                    as: 'Subcategory',
                    where: {id: 77},
                    include: [{
                        model: mockSequelizeModel('ProductCategory'),
                        where: {name: 'Foo'}
                    }]
                }]
            };

            expect(SequelizeIncludeGenerator.buildIncludeGraph(filters, basicIncludeGraph))
                .to.eql(goodOutput);
        });

        it('returns complex nested results', () => {
            let filters = {
                'Subcategory.id': 77,
                'Subcategory.ProductCategory.id': 14
            };

            let goodOutput = {
                include: [{
                    model: mockSequelizeModel('ProductSubcategory'),
                    as: 'Subcategory',
                    where: {
                        id: 77
                    },
                    include: [{
                        model: mockSequelizeModel('ProductCategory'),
                        where: {
                            id: 14
                        }
                    }]
                }]
            };

            expect(SequelizeIncludeGenerator.buildIncludeGraph(filters, basicIncludeGraph))
                .to.eql(goodOutput);
        });

        it('works with string-based models', () => {
            let include = [
                {
                    model: 'ProductSubcategory',
                    as: 'Subcategory',
                    include: ['ProductCategory']
                }
            ];

            let goodOutput = {
                include: [{
                    model: mockSequelizeModel('ProductSubcategory'),
                    as: 'Subcategory',
                    include: [{
                        model: mockSequelizeModel('ProductCategory')
                    }]
                }]
            };

            expect(SequelizeIncludeGenerator.buildIncludeGraph({}, include)).to.eql(goodOutput);
        });
    });

    describe('#_formatFilters', () => {
        it('formats a simple fitler object', () => {
            let filters = {
                name: 'Converse'
            };

            let goodResult = {
                '': {
                    'name': 'Converse'
                }
            };

            expect(SequelizeIncludeGenerator._formatFilters(filters)).to.eql(goodResult);
        });

        it('formats a complex filter object', () => {
            let fitlers = {
                'ProductCategory.ProductSubcategory.id': 1
            };

            let goodResult = {
                'ProductCategory.ProductSubcategory': {
                    'id': 1
                }
            };

            expect(SequelizeIncludeGenerator._formatFilters(fitlers)).to.eql(goodResult);
        });
    });
});
